"""
Software Copyright Notice
Copyright 2022 Praelexis (Pty) Ltd.
All rights are reserved

Copyright exists in this computer program and it is protected by
copyright law and by international treaties. The unauthorised use,
reproduction or distribution of this computer program constitute acts
of copyright infringement and may result in civil and criminal
penalties. Any infringement will be prosecuted to the maximum extent
possible.

Praelexis (Pty) Ltd chooses the following address for delivery of all
legal proceedings and notices:
   7 Neutron Avenue,
   Stellenbosch,
   7600,
   South Africa.
"""

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="nappi_reader",
    version="0.0.0",
    author="Praelexis",
    author_email="admin@praelexis.com",
    license="PROPRIETARY",
    description="Process a National Pharmaceutical Product Interface (NAPPI) file",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://praelexis.com",
    python_requires=">=3.7",
)

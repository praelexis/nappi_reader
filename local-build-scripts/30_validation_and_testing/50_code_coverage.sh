#!/bin/bash
# Software Copyright Notice
# Copyright 2022 Praelexis (Pty) Ltd.
# All rights are reserved
#
# Copyright exists in this computer program and it is protected by
# copyright law and by international treaties. The unauthorised use,
# reproduction or distribution of this computer program constitute acts
# of copyright infringement and may result in civil and criminal
# penalties. Any infringement will be prosecuted to the maximum extent
# possible.
#
# Praelexis (Pty) Ltd chooses the following address for delivery of all
# legal proceedings and notices:
#    7 Neutron Avenue,
#    Stellenbosch,
#    7600,
#    South Africa.

. ${build_scripts}/utils/useful_funcs.sh

info "# Pipeline 'using pytest and pytest-cov to run tests'"

# We check that pytest, pytest-cov & pytest-xdist are all installed
if ! pip show -q pytest; then
   fail "## Pipelines FAILURE: Config chooses pytest runner, but pytest is not installed"
   exit 1
fi

if ! pip show -q pytest-cov; then
   fail "## Pipelines FAILURE: Config chooses pytest runner, but required package pytest-cov is not installed"
   exit 1
fi

if ! pip show -q pytest-xdist; then
   fail "## Pipelines FAILURE: Config chooses pytest runner, but required package pytest-xdist is not installed"
   exit 1
fi

# Get the configuration file
if [ -f .coveragerc ]; then
    rcfile=".coveragerc"
else
  warn "No .coveragerc file, using the default config"
  rcfile="${build_scripts}/python/default-dot-files/.coveragerc"
fi

# Run tests and code coverage
pytest --cov-config=${rcfile} --cov=$PKG_NAME --cov-report term-missing --cov-report html -n auto --dist loadscope
res_test=$?

if [ $res_test -ne 0 ]
then
  fail "## Pipelines FAILURE: Testing step failed"
  exit 1
fi

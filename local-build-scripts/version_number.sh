#!/bin/bash
# Software Copyright Notice
# Copyright 2022 Praelexis (Pty) Ltd.
# All rights are reserved
#
# Copyright exists in this computer program and it is protected by
# copyright law and by international treaties. The unauthorised use,
# reproduction or distribution of this computer program constitute acts
# of copyright infringement and may result in civil and criminal
# penalties. Any infringement will be prosecuted to the maximum extent
# possible.
#
# Praelexis (Pty) Ltd chooses the following address for delivery of all
# legal proceedings and notices:
#    7 Neutron Avenue,
#    Stellenbosch,
#    7600,
#    South Africa.

# Programmatically set the version number

export VERSION=0.0.1
